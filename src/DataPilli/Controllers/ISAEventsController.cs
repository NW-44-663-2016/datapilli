using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using DataPilli.Models;

namespace DataPilli.Controllers
{
    public class ISAEventsController : Controller
    {
        private AppDbContext _context;

        public ISAEventsController(AppDbContext context)
        {
            _context = context;    
        }

        // GET: ISAEvents
        public IActionResult Index()
        {
            return View(_context.ISAEvents.ToList());
        }

        // GET: ISAEvents/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            ISAEvent iSAEvent = _context.ISAEvents.Single(m => m.eventID == id);
            if (iSAEvent == null)
            {
                return HttpNotFound();
            }

            return View(iSAEvent);
        }

        // GET: ISAEvents/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ISAEvents/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ISAEvent iSAEvent)
        {
            if (ModelState.IsValid)
            {
                _context.ISAEvents.Add(iSAEvent);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(iSAEvent);
        }

        // GET: ISAEvents/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            ISAEvent iSAEvent = _context.ISAEvents.Single(m => m.eventID == id);
            if (iSAEvent == null)
            {
                return HttpNotFound();
            }
            return View(iSAEvent);
        }

        // POST: ISAEvents/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(ISAEvent iSAEvent)
        {
            if (ModelState.IsValid)
            {
                _context.Update(iSAEvent);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(iSAEvent);
        }

        // GET: ISAEvents/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            ISAEvent iSAEvent = _context.ISAEvents.Single(m => m.eventID == id);
            if (iSAEvent == null)
            {
                return HttpNotFound();
            }

            return View(iSAEvent);
        }

        // POST: ISAEvents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            ISAEvent iSAEvent = _context.ISAEvents.Single(m => m.eventID == id);
            _context.ISAEvents.Remove(iSAEvent);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
