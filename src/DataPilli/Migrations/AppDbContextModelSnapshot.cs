using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using DataPilli.Models;

namespace DataPilli.Migrations
{
    [DbContext(typeof(AppDbContext))]
    partial class AppDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataPilli.Models.ISAEvent", b =>
                {
                    b.Property<int>("eventID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("LocationID");

                    b.Property<DateTime>("eventDate");

                    b.Property<double>("eventDuration");

                    b.Property<string>("eventLocation");

                    b.Property<string>("eventName")
                        .IsRequired();

                    b.Property<string>("eventType");

                    b.HasKey("eventID");
                });

            modelBuilder.Entity("DataPilli.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("DataPilli.Models.ISAEvent", b =>
                {
                    b.HasOne("DataPilli.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });
        }
    }
}
