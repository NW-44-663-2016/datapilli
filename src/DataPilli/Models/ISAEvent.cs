﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace DataPilli.Models
{
    public class ISAEvent
    {
        [ScaffoldColumn(false)]
        [Key]
        public int eventID { get; set; }

        [Required]
        [Display(Name = "Event Name")]
        public String eventName { get; set; }

        [Display(Name = "Event Type")]
        public string eventType { get; set; }

        [RegularExpression(@"/(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d/", ErrorMessage = "Invalid date")]
        [Display(Name = "date")]
        public DateTime eventDate { get; set; }

        [Display(Name = "Location")]
        public string eventLocation { get; set; }

        [Range(typeof(double), "0.50", "24.00")]
        [Display(Name = "Duration")]
        public double eventDuration { get; set; }

        [ScaffoldColumn(true)]
        public int? LocationID { get; set; }

        public virtual Location Location { get; set; }

        public static List<ISAEvent> ReadAllFromCSV(string filepath)
        {
            List<ISAEvent> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => ISAEvent.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static ISAEvent OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            ISAEvent item = new ISAEvent();

            int i = 0;
            
            item.eventName = Convert.ToString(values[i++]);
            item.eventType = Convert.ToString(values[i++]);
            item.eventDate = Convert.ToDateTime(values[i++]);
            item.eventLocation = Convert.ToString(values[i++]);
            item.eventDuration = Convert.ToDouble(values[i++]);
            item.LocationID = Convert.ToInt32(values[i++]);

            return item;
        }

    }
}
