﻿using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataPilli.Models
{
    public class AppDbContext : DbContext
    {
        public DbSet<Location> Locations { get; set; }

        public DbSet<ISAEvent> ISAEvents { get; set; }
    }
}
